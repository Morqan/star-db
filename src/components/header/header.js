import React from 'react';

import './header.css';

const Header = () => {
  return (
    <header>
      <div className="">
        <div className="header d-flex">
          <h3>
            <a href="#star-db">
              Star DB
            </a>
          </h3>
          <ul className="d-flex">
            <li>
              <a href="#people">People</a>
            </li>
            <li>
              <a href="#planets">Planets</a>
            </li>
            <li>
              <a href="#starship">Starships</a>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};

export default Header;